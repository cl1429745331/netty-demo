package com.example.nettyexample;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName NettyBasicServerExample.java
 * @Description TODO
 * @createTime 2022年03月16日 23:02:00
 */
public class NettyBasicServerExample {

    /**
     * 开发一个主从多reactor多线程模型的服务
     *
     * @param args
     */
    public static void main(String[] args) {

        // Netty 的服务端编程要从EventLoopGroup开始
        // 我们需要创建两个 EventLoopGroup
        // 一个是boss专门用来接受连接，可以理解为处理accept事件
        // 另一个是worker，可以关注除了accept之外的其他事件，处理子任务
        // 需要注意的是boss线程一般只设置一个，就算设置多个也只会用到一个
        // worker线程通常要根据服务器进行调优，如果不写默认是CPU的两倍

        //主线程（只需要1个）
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        //工作线程组
        EventLoopGroup workGroup = new NioEventLoopGroup(4);
        //构建Netty Server的API
        ServerBootstrap bootstrap = new ServerBootstrap();
        //Bootstrap
        bootstrap.group(bossGroup, workGroup)
                //指定epoll模型
                .channel(NioServerSocketChannel.class)
                .handler(new LoggingHandler(LogLevel.INFO))//设置服务端ServerSocketChannel对应的handle
                //具体的客户端工作处理类,负责处理相关SocketChannel的IO就绪事件
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        //心跳的hander
                        //编解码
                        //协议处理
                        //消息处理
                        socketChannel.pipeline()
                                .addLast(new NormalMessageHandler());//处理IO事件
                    }
                });
        try {
            ChannelFuture channelFuture = bootstrap.bind(8080).sync();//同步阻塞等待客户端连接
            System.out.println("Netty Server Started Success");
            channelFuture.channel().closeFuture().sync();//同步等待服务端监听端口关闭
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //释放资源
            workGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}
