package com.rpc.example;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName IUserService.java
 * @Description TODO
 * @createTime 2022年04月23日 19:31:00
 */
public interface IUserService {

    /**
     * 保存用户信息
     */
    String saveUser(String name);

}
