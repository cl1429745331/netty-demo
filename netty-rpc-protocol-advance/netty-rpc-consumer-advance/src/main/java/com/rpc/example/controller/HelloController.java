package com.rpc.example.controller;

import com.rpc.example.IUserService;
import com.rpc.example.annotation.RemoteReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName HelloController.java
 * @Description TODO
 * @createTime 2022年04月28日 20:54:00
 */
@RestController
public class HelloController {

    @RemoteReference
    private IUserService userService;

    @GetMapping("/say")
    public String say() {
        return userService.saveUser("cc");
    }
}
