package com.rpc.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName NettyConsumerMain.java
 * @Description TODO
 * @createTime 2022年04月28日 21:46:00
 */
@ComponentScan(basePackages = {"com.rpc.example.spring.reference","com.rpc.example.controller","com.rpc.example.annotation"})
@SpringBootApplication
public class NettyConsumerMain {

    public static void main(String[] args) {
        SpringApplication.run(NettyConsumerMain.class, args);
    }
}
