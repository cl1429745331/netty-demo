package com.rpc.example.spring.service;

import com.rpc.example.core.RpcRequest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName Mediator.java
 * @Description 委托对象
 * @createTime 2022年04月28日 20:23:00
 */
public class Mediator {

    // 定义好映射关系
    // 管理所有bean 和 method的关系
    public static Map<String, BeanMethod> beanMethodMap = new ConcurrentHashMap<>();

    // 构造单例方法
    private volatile static Mediator instance = null;

    private Mediator() {
    }

    public static Mediator getInstance() {
        if (instance == null) {
            synchronized (Mediator.class) {
                if (instance == null) {
                    instance = new Mediator();
                }
            }
        }

        return instance;
    }

    // 根据请求从map里面获取关系进行调用
    public Object processor(RpcRequest request) throws InvocationTargetException, IllegalAccessException {
        String key = request.getClassName() + "." + request.getMethodName();
        BeanMethod beanMethod = beanMethodMap.get(key);
        if (beanMethod == null) {
            return null;
        }
        Object object = beanMethod.getBean();
        Method method = beanMethod.getMethod();
        return method.invoke(object, request.getParams());
    }
}
