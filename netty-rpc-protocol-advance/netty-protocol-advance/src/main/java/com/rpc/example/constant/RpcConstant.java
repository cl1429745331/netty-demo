package com.rpc.example.constant;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName RpcConstant.java
 * @Description TODO
 * @createTime 2022年04月23日 20:30:00
 */
public class RpcConstant {

    public final static short MAGIC = 0xca; // 魔树

    public final static int HEAD_TOTAL_LEN = 16; //header总的字节数量

}
