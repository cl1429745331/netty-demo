package com.example.code;

import com.example.code.MessageRecordDecode;
import com.example.code.MessageRecordEncode;
import com.example.opcode.OpCode;
import com.example.protocol.Header;
import com.example.protocol.MessageRecord;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName MainTest.java
 * @Description TODO
 * @createTime 2022年04月18日 17:33:00
 */
public class MainTest {

    public static void main(String[] args) throws Exception {
        // ChannelHandler 单元测试
        EmbeddedChannel channel = new EmbeddedChannel(
                new LengthFieldBasedFrameDecoder
                        (1024 * 1024,
                                9,
                                4,
                                0,
                                0),
                new LoggingHandler(),
                new MessageRecordEncode(),
                new MessageRecordDecode()
        );

        // 定义消息内容
        Header header = new Header();
        header.setSessionId(123456);
        header.setReqType(OpCode.REQ.code());

        MessageRecord record = new MessageRecord();
        record.setHeader(header);
        record.setBody("Hello World");

//        channel.writeOutbound(record); // 写出去 编码

//        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
//        new MessageRecordEncode().encode(null, record, buf);
//        channel.writeInbound(buf); // 读取消息内容 解码

        // 测试拆包粘包的问题
        // 编码得到了一个Bytebuf
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        new MessageRecordEncode().encode(null, record, buf);

        ByteBuf bb1 = buf.slice(0, 7); //数据包1
        ByteBuf bb2 = buf.slice(7, buf.readableBytes() - 7); //数据包2
        bb1.retain();//新增引用需要调用retain方法 ，释放引用需要调用release方法

        //这边是接受了两个包 第一个包接受后，由于长度不够，会等后面的包
        channel.writeInbound(bb1);
        channel.writeInbound(bb2);
    }
}
