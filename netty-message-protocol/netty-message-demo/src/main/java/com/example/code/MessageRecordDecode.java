package com.example.code;

import com.example.protocol.Header;
import com.example.protocol.MessageRecord;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.List;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName MessageRecordDecode.java
 * @Description TODO
 * @createTime 2022年04月18日 15:13:00
 */
public class MessageRecordDecode extends ByteToMessageDecoder {

    /**
     * 协议报文
     * sessionId / reqType / Content-length / Content
     *
     * @param channelHandlerContext
     * @param byteBuf
     * @param list
     * @throws Exception
     */
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        MessageRecord record = new MessageRecord();
        //ByteBuf 表示接收到的消息报文
        Header header = new Header();
        header.setSessionId(byteBuf.readLong()); //读取8个字节
        header.setReqType(byteBuf.readByte()); //消息类型
        header.setLength(byteBuf.readInt()); //读取四个字节长度，消息内容长度
        record.setHeader(header);
        // 判断消息长度
        if (header.getLength() > 0) {
            byte[] contents = new byte[header.getLength()];
            byteBuf.readBytes(contents);
            /**
             * Java原生的对象流 反序列化
             */
            ByteArrayInputStream bis = new ByteArrayInputStream(contents);
            ObjectInputStream ios = new ObjectInputStream(bis);
            record.setBody(ios.readObject());//得到一个反序列化的数据
            System.out.println("反序列化出来的结果：" + record);
            list.add(record);
        } else {
            System.out.println("消息内容为空");
        }
    }
}
