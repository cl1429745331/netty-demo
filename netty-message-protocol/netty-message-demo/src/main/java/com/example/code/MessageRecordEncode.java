package com.example.code;

import com.example.protocol.Header;
import com.example.protocol.MessageRecord;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName MessageRecordEncode.java
 * @Description TODO
 * @createTime 2022年04月18日 15:13:00
 */
public class MessageRecordEncode extends MessageToByteEncoder<MessageRecord> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, MessageRecord messageRecord, ByteBuf byteBuf) throws Exception {
        System.out.println("==========开始进行消息编码==================");
        Header header = messageRecord.getHeader(); //得到消息协议的header部分
        byteBuf.writeLong(header.getSessionId()); //写8个字节的sessionid
        byteBuf.writeByte(header.getReqType()); //1个字节的消息类型

        Object body = messageRecord.getBody();
        if (body != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(body);
            byte[] bytes = bos.toByteArray();//
            byteBuf.writeInt(bytes.length); //写消息的长度
            byteBuf.writeBytes(bytes);
        } else {
            byteBuf.writeInt(0); //表示消息长度为0
        }
    }
}
