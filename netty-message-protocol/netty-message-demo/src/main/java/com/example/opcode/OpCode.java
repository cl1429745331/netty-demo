package com.example.opcode;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName OpCode.java
 * @Description TODO
 * @createTime 2022年04月18日 15:11:00
 */
public enum OpCode {

    REQ((byte) 0),
    RES((byte) 1),
    PING((byte) 2),
    PONG((byte) 3);

    private byte code;

    OpCode(byte code) {
        this.code = code;
    }

    public byte code() {
        return this.code;
    }

}