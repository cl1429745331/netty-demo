package com.example.protocol;

import lombok.Data;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName MessageRecord.java
 * @Description TODO
 * @createTime 2022年04月18日 15:10:00
 */
@Data
public class MessageRecord {

    private Header header;

    private Object body;

}
