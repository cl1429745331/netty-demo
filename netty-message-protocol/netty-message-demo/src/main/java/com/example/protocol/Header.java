package com.example.protocol;

import lombok.Data;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName Header.java
 * @Description TODO
 * @createTime 2022年04月18日 15:05:00
 */
@Data
public class Header {

    private long sessionId;  //会话id，8个字节

    private byte reqType;  //消息类型， 1个字节

    private int length; //消息体的长度  4个字节

}
