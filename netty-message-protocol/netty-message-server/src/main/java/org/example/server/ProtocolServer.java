package org.example.server;

import com.example.code.MessageRecordDecode;
import com.example.code.MessageRecordEncode;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName ProtocolServer.java
 * @Description TODO
 * @createTime 2022年04月18日 19:01:00
 */
public class ProtocolServer {

    public static void main(String[] args) {
        EventLoopGroup boss = new NioEventLoopGroup();
        EventLoopGroup work = new NioEventLoopGroup();
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(boss, work)
                .channel(NioServerSocketChannel.class) // 可能： 工厂方法通过反射构建实例
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    // 针对客户端连接来设置Pipeline
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline()
                                .addLast(new LengthFieldBasedFrameDecoder(1024 * 1024,
                                        9,
                                        4,
                                        0,
                                        0))
                                .addLast(new MessageRecordEncode())
                                .addLast(new MessageRecordDecode())
                                .addLast(new ServerHandler());
                    }
                });

        try {
            // 类似并发编程中的CompletableFuture
            ChannelFuture channelFuture = bootstrap.bind(8080).sync();
            System.out.println("Protocol server start success");
            channelFuture.channel().closeFuture().sync();//同步等待关闭事件
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            work.shutdownGracefully();
            boss.shutdownGracefully();
        }
    }
}
