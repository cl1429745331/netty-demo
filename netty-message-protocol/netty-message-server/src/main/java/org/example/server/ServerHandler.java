package org.example.server;

import com.example.opcode.OpCode;
import com.example.protocol.MessageRecord;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName ServerHandler.java
 * @Description TODO
 * @createTime 2022年04月18日 19:27:00
 */
public class ServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        // ServerHandler 应该是SocketChannel 触发的
        // 这时的msg应该是解码器获取的对象
        MessageRecord record = (MessageRecord) msg;
        System.out.println("Server Receive Message:" + record);
        record.setBody("Server Response Message");
        record.getHeader().setReqType(OpCode.RES.code());
        ctx.writeAndFlush(record);//将信息写回客户端
    }
}
