package org.example;

import com.example.code.MessageRecordDecode;
import com.example.code.MessageRecordEncode;
import com.example.opcode.OpCode;
import com.example.protocol.Header;
import com.example.protocol.MessageRecord;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName ProtocolClient.java
 * @Description TODO
 * @createTime 2022年04月18日 19:40:00
 */
public class ProtocolClient {

    public static void main(String[] args) {
        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline()
                                .addLast(new LengthFieldBasedFrameDecoder(1024 * 1024,
                                        9,
                                        4,
                                        0,
                                        0))
                                .addLast(new MessageRecordEncode())
                                .addLast(new MessageRecordDecode())
                                .addLast(new ClientHandler());
                    }
                });

        try {
            ChannelFuture future = bootstrap.connect(new InetSocketAddress("localhost", 8080)).sync();
            Channel channel = future.channel();
            for (int i = 0; i < 100; i++) {
                MessageRecord record = new MessageRecord();
                Header header = new Header();
                header.setSessionId(10001);
                header.setReqType(OpCode.REQ.code());
                record.setHeader(header);
                String body = "我是请求数据：" + i;
                record.setBody(body);
                channel.writeAndFlush(record);
            }
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }

    }
}
