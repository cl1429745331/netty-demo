package com.example.nettypacketexample;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName ByteBufCreateExample.java
 * @Description TODO
 * @createTime 2022年04月16日 14:41:00
 */
public class ByteBufCreateExample {

    public static void main(String[] args) {
//        // 由JVM管理 堆内存
//        ByteBuf byteBuf = ByteBufAllocator.DEFAULT.heapBuffer();
//        // 需要自己管理内存 堆外内存  （读写性能高）
//        ByteBufAllocator.DEFAULT.directBuffer();
//
//        // ByteBuf 使用了池化技术 （可以打印实例查看效果）
//        // 可以选择是否开启池化技术
//        // JVM ：-Dio.netty.allocator.type = {unpooled | polled}
//        System.out.println(byteBuf);

        ByteBuf buf = ByteBufAllocator.DEFAULT.heapBuffer();
        buf.writeBytes(new byte[]{1, 2, 3, 4});
        log(buf);
        buf.writeInt(5);
        log(buf);
        System.out.println("开始进行读取操作");
        buf.markReaderIndex(); //标记索引位置.  markWriterIndex()
        byte b = buf.readByte();
        System.out.println(b);
        buf.resetReaderIndex(); //重新回到标记位置
        log(buf);
    }

    private static void log(ByteBuf buf) {
        StringBuilder sb = new StringBuilder();
        sb.append(" read index:").append(buf.readerIndex());  //读索引
        sb.append(" write index:").append(buf.writerIndex()); //写索引
        sb.append(" capacity :").append(buf.capacity()); //容量
        ByteBufUtil.appendPrettyHexDump(sb, buf);
        System.out.println(sb.toString());
    }
}
