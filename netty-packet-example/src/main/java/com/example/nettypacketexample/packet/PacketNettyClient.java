package com.example.nettypacketexample.packet;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringEncoder;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName PacketNettyClient.java
 * @Description 客户端
 * @createTime 2022年04月16日 20:35:00
 */
public class PacketNettyClient {

    public static void main(String[] args) {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(eventLoopGroup)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline()
                                //表示传输消息的时候，在消息报文中增加4个字节的length。->将长度添加到发送的ByteBuf
                                .addLast(new LengthFieldPrepender(4, 0, false))
//                                .addLast(new FixedLengthFrameDecoder(36))
//                                .addLast(new DelimiterBasedFrameDecoder
//                                        (10,true,true,delimiter))
                                .addLast(new StringEncoder())
                                .addLast(new ChannelInboundHandlerAdapter() {
                                    @Override
                                    public void channelActive(ChannelHandlerContext ctx) throws Exception {

                                        ctx.writeAndFlush("i am first request");
                                        ctx.writeAndFlush("i am second request");
//                                        super.channelActive(ctx);
                                    }
                                });
                    }
                });
        try {
            ChannelFuture future = bootstrap.connect("localhost", 8080).sync();
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            eventLoopGroup.shutdownGracefully();
        }
    }

}
