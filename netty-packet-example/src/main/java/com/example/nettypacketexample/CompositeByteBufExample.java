package com.example.nettypacketexample;

import io.netty.buffer.*;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName CompositeByteBufExample.java
 * @Description TODO
 * @createTime 2022年04月16日 20:08:00
 */
public class CompositeByteBufExample {

    // 合并 也是零拷贝的实现
    public static void main(String[] args) {
        ByteBuf header = ByteBufAllocator.DEFAULT.buffer();
        header.writeBytes(new byte[]{1, 2, 3, 4, 5});
        ByteBuf body = ByteBufAllocator.DEFAULT.buffer();
        body.writeBytes(new byte[]{6, 7, 8, 9, 10});
/*        ByteBuf total= Unpooled.buffer(header.readableBytes()+body.readableBytes());
        total.writeBytes(header);
        total.writeBytes(body);*/
        //从逻辑成面构建了一个总的buf数据。
        //第二个零拷贝实现
//        CompositeByteBuf compositeByteBuf = Unpooled.compositeBuffer();
//        //其中第一个参数是 true, 表示当添加新的 ByteBuf 时, 自动递增 CompositeByteBuf 的 writeIndex.
//        //默认是false，也就是writeIndex=0，这样的话我们不可能从compositeByteBuf中读取到数据。
//        compositeByteBuf.addComponents(true, header, body);
//        log(compositeByteBuf);


        //Unpooled
        ByteBuf total = Unpooled.wrappedBuffer(header, body);
        log(total);
        header.setByte(2, 9);
        log(total);
    }

    private static void log(ByteBuf buf) {
        StringBuilder sb = new StringBuilder();
        sb.append(" read index:").append(buf.readerIndex());  //读索引
        sb.append(" write index:").append(buf.writerIndex()); //写索引
        sb.append(" capacity :").append(buf.capacity()); //容量
        ByteBufUtil.appendPrettyHexDump(sb, buf);
        System.out.println(sb.toString());
    }

}
