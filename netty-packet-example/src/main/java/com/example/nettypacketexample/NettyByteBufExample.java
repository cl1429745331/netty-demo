package com.example.nettypacketexample;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;

import java.nio.charset.StandardCharsets;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName NettyByteBufExample.java
 * @Description TODO
 * @createTime 2022年04月16日 14:13:00
 */
public class NettyByteBufExample {

    public static void main(String[] args) {
        // 构建一个ByteBuf实例
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        System.out.println("==========before==========");
        log(buf);

        // 构建字符串数据
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 40; i++) {
            builder.append("-" + i);
        }

        // 将字符串写入ByteBuf里
        buf.writeBytes(builder.toString().getBytes(StandardCharsets.UTF_8));
        System.out.println("==========after==========");

        //可以通过API选择读取多少个字节
        buf.readByte(); //1个字节
        buf.readShort(); //2个字节
        log(buf);
    }

    private static void log(ByteBuf byteBuf) {
        StringBuilder builder = new StringBuilder();
        builder.append(" read index :").append(byteBuf.readerIndex()); // 读索引
        builder.append(" write index :").append(byteBuf.writerIndex()); // 写索引
        builder.append(" capacity :").append(byteBuf.capacity()); // 容量

        // 会将ByteBuf里的数据按照16进制格式化后存入字符串中
        ByteBufUtil.appendPrettyHexDump(builder, byteBuf);
        // 打印
        System.out.println(builder.toString());
    }
}
